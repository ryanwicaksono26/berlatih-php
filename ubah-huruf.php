<?php
function ubah_huruf($string){
    $result = '';
    $huruf = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
    $panjang_huruf = strlen($string);
    $split_huruf = str_split($string, 1);
    for($i = 0; $i < count($split_huruf); $i++){
        for($j = 0; $j < count($huruf); $j++){
            if($split_huruf[$i] == $huruf[$j]){
                $result .= $huruf[$j+1];
            }
        }
    }
    return $result;    
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo "<br>";
echo ubah_huruf('developer'); // efwfmpqfs
echo "<br>";
echo ubah_huruf('laravel'); // mbsbwfm
echo "<br>";
echo ubah_huruf('keren'); // lfsfo
echo "<br>";
echo ubah_huruf('semangat'); // tfnbohbu
echo "<br>";
?>