<?php
function tentukan_nilai($number)
{
    if($number >= 85 && $number <= 100){
        echo "Sangat Baik";
    } else if ($number >=70 && $number < 85){
        echo "Baik";
    } else if ($number >= 60 && $number < 70){
        echo "Cukup";
    } else {
        echo "kurang";
    }
}

//TEST CASES
echo "Nilai 98: ";
echo tentukan_nilai(98); //Sangat Baik
echo "<br>";
echo "Nilai 76: ";
echo tentukan_nilai(76); //Baik
echo "<br>";
echo "Nilai 67: ";
echo tentukan_nilai(67); //Cukup
echo "<br>";
echo "Nilai 43: ";
echo tentukan_nilai(43); //Kurang
echo "<br>";
?>